from flask_sqlalchemy import SQLAlchemy
from .models import db, init_db_command
from .views import bp
from flask import Flask, current_app
from os import getenv


def create_app(test_config=None):

    app = Flask(__name__, instance_relative_config=True)

    if test_config is None:  # pragma: no cover
        app.config.from_object(
            getenv("APP_SETTINGS")
            if getenv("APP_SETTINGS") is not None
            else "config.DevelopmentConfig"
        )
    else:
        app.config.from_object(test_config)
    app.config.from_mapping(SQLALCHEMY_TRACK_MODIFICATIONS=False)

    db.init_app(app)
    app.cli.add_command(init_db_command)

    app.register_blueprint(bp)

    return app
